#!/bin/bash

# This script creates an installer product package for the Yiddish keyboard
#
# Step 1: Use pkgbuild to create the initial build package
# Step 2: Use productbuild in analysis mode to create a working distribution.plist
# Step 3: Edit distribution.plist to include licence and readme. (done manually)
# Step 4: Use productbuild to create a distributable pkg
#
# Author: Daniel Nemenyi
# Organisation: okey_kompyuter
# Licence: GPL3

version='2.0'

build='ok_k-yiddish-keyboard.pkg'
bundle='ok_k-yiddish-klaviatur.bundle'
dest=~/Library/Keyboard\ Layouts
dmg="Ok-Kompyuter-Yiddish-Keyboard-$version.dmg"
plist='tmp/distribution.plist'
product=tmp/dmg/Install\ Okey\ Kompyuter\ Yiddish\ Keyboard.pkg
src='src/macos'

# STEP 1
if [ -d "tmp" ]; then
    printf "tmp/ exists so won't create it.\n"
else
    mkdir -p tmp/dmg \
	&& printf "Created tmp/dmg.\n"
fi
    

# Now Let's build our initial build package
printf "Now to pkgbuild, to build the initial install pkg...\n"
pkgbuild --install-location "$dest" \
	 --component $src/$bundle \
	 $build \
    && printf "Done with pkgbuild.\n"

# Step 2
# If the product package doesn't already exist, then it needs to
# be initiated in analysis mode so that we get a working
# distribution.plist ribution.plist. Note this will clobber any existing
# distribution.plist
printf "Now to productbuild to create a plist...\n"
productbuild --synthesize \
	     --package $build \
	     $plist

# Fuck macOS sed! Use gnu-sed (`brew install gnu-sed`) to insert licence and readme
printf "Now to sed to add readme and licence to plist...\n"
gsed -i 's|<\/choice>|</choice>\
    <license file="LICENSE.html"/>\
    <readme file="README.md"/>|g' $plist \
    && echo "sed success"

# STEP 4
printf "Now for the final productbuild...\n"
productbuild \
    --distribution $plist \
    --resources . \
    --version $version \
    --package-path $build \
    "$product" \
    && printf "Finished with productbuild!\n"

printf "Now to create the dmg..."
hdiutil create \
	-volname "Okey Kompyuter Yiddish Keyboard" \
	-srcfolder "$product" \
	-ov \
	$dmg \
	&& printf "Successfully created $dmg\n"

printf "Now to clean up..."
# clean up
rm -r $build \
    && printf "Deleted $build"
rm -r tmp \
    && printf "Deleted tmp/"

printf "All done! Check out $dmg"
